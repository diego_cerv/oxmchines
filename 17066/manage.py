import pymysql
import os 
import sys
import re
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

Arg = sys.argv

host = 'localhost'
user = 'root'
password = 'oxmachines'

def ipValidation(ip_adress):
    ipRegExp = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$'''
    
    if re.search(ipRegExp,ip_adress):
        #print("\nYou have entered a valid IP\n")
        return True
    else:
        #print("\nInvalid IP, Please enter a valid one...\n")
        return False 

def macAdressValidation(mac_adress):
    macRegExp_1 = '''^([0-9A-F]{2}[-:]){5}[0-9A-F]{2}$'''
    macRegExp_2 = '''^([0-9a-f]{2}[-:]){5}[0-9a-f]{2}$'''

    if re.search(macRegExp_1,mac_adress) or re.search(macRegExp_2,mac_adress):
        #print("\nYou have entered a valid MAC ADRESS\n")
        return True
    else:
        #print("\nInvalid MAC ADRESS, Please enter a valid one...\n")
        return False

def boolValidation(val):
    if val == '1' or val == '0' or val == 'True' or val == 'False':
        return True
    else:
        print("\nInvalid BOOL value\n" )
        return False

def intValidation(val):
    try:
        int(val)
    except ValueError:
        return("Invalid int value")
    else:
        return("Data validated")

def stringValidation(val):
    if isinstance(val,str):
        return True
    else:
        return False

def SearchProject(pjname):
    start=os.getcwd()
    for roots,dirs,files in os.walk(start):
        if pjname in dirs:
            return 1,os.path.join(roots,pjname)
    return ("ERROR: Project directory doesn't exist",pjname)

def typeValidation(type,val):
    if type == 'ip':
        return ipValidation(val)
    elif type == 'mac':
        return macAdressValidation(val)
    elif type == 'bool':
        return boolValidation(val)
    elif type == 'int' or type == 'id':
        return intValidation(val)
    elif type == 'str':
        return stringValidation(val)
    else:
        print('No type variable found...')

def readSplitFile(pathFile):
    f = open(pathFile,'r') 
    l = [[str(num) for num in line.split(',')] for line in f]
    
    for k in range (len(l)):
            for j in range (len(l[k])):
                l[k][j]=l[k][j].replace("\n","")
    return l

def readSplitFile_1(pathFile):
    f = open(pathFile,'r') 
    l = [[str(num) for num in line.split(',')] for line in f]

    if len(l) == 1:
        return l

    else:
        rowlen = len(l)
        collen = len(l[0])

        for row in range(rowlen):
            l[row][collen-1] = l[row][collen-1].replace('\n', '')
        return l

def readSplitLineFile(pathFile):
    f = open(pathFile,'r') 
    content = f.read()
    lst = content.split(',')
    return lst

def dbargs():
    bkp="DB.BKP"
    bkpRoot_exists,bkpRoot=SearchProject(bkp)

    if bkpRoot_exists:
        dbNameList = os.listdir(bkpRoot)
    
    return bkpRoot,dbNameList

def conn(host,user,password):
    conn = pymysql.Connect(host,user,password)
    c = conn.cursor()

    try:
        if c.connection:
            print("Connection to MariaDB is succesfull!")
            return conn

    except Exception as e:
        print(str(e))

def queryExe(conn,query):
    c = conn.cursor()
    try:
        if c.connection:
            c.execute(query)
            return c
        else:
            print("Trying to reconnect...")
            conn=conn(host,user,password)
    except Exception as e:
        return str(e)

def createDatabase(conn,dbName):
    sqlSTM = 'CREATE DATABASE ' + dbName + ';'
    queryExe(conn,sqlSTM)
    print('DATABASE --{}-- created succesfully!'.format(dbName))
    return sqlSTM

def dropDatabase(conn,dbName):
    sqlSTM = 'DROP DATABASE ' + dbName + ';'
    queryExe(conn,sqlSTM)
    print('DATABASE --{}-- dropped succesfully!'.format(dbName))
    return sqlSTM

def useDatabase(conn,dbName):
     dbSTM = 'USE ' + dbName + ';'
     queryExe(conn,dbSTM)
     return dbSTM

def dataTypeFields(dataTypeList):

    sqlType = []

    for dt in dataTypeList:
        if dt == 'id':
            sqlType += ['INT AUTO_INCREMENT']
        elif dt == 'int':
            sqlType += ['INT']
        elif dt == 'bool':
            sqlType.append('BOOL')
        elif dt == 'ip':
            sqlType.append('VARCHAR(100)')
        elif dt == 'mac':
            sqlType.append('VARCHAR(100)')
        elif dt == 'str':
            sqlType.append('VARCHAR(65532)')
        else:
            print('No data type found...')
    
    return sqlType

def createTable(conn,bkpFolder,dbName,csvName):

    tabPath = os.path.join(bkpFolder,dbName,csvName)

    tab = readSplitFile_1(tabPath)

    fieldList = tab[0]
    typeList = dataTypeFields(tab[1])

    startSTM = 'CREATE TABLE {}('.format(os.path.splitext(csvName)[0])

    if len(fieldList) == len(typeList):
        tabContentList = []
        for fld,tp in zip(fieldList,typeList):
            tabContentList.append('{} {}, '.format(fld,tp))

        endSTR = 'PRIMARY KEY ({}))'.format(fieldList[0])

        fieldStr = ''
        sqlFields = fieldStr.join(tabContentList)

        finalSTM = startSTM + sqlFields + endSTR + ';'

        queryExe(conn,finalSTM)

        print('TABLE --{}-- created succesfully!'.format(os.path.splitext(csvName)[0]))
        InsertIntoTable(tabPath,dbName)
        
    else:
        print('Fields and Type lenghts of TABLE --{}-- do not match...\n\n'.
        format(os.path.splitext(csvName)[0]))

    return finalSTM

def sqlCreateDB(conn,bkpFolder,dbName):

    db_existence = 'SHOW DATABASES;'
    cursor = queryExe(conn,db_existence)
    ex_result = cursor.fetchall()
    resultList = [i[0] for i in ex_result]


    if dbName in resultList:
        print('\nDatabase {} already exists...\n\n'.format(dbName))

    else:

        sqlFileList = []

        print('Creating new DATABASE from BACK.UP folder files...\n\n')

        sqlFileList.append(createDatabase(conn,dbName))

        print('Creating DATABASE TABLES...\n\n')

        sqlFileList.append(useDatabase(conn,dbName))

        try:
            
            tabPath = os.path.join(bkpFolder,dbName)
            dbNameList = []

            for names in os.listdir(tabPath):
                if names.endswith('.csv'):
                    dbNameList.append(names) 

            for tb in dbNameList:
                sqlFileList.append(createTable(conn,bkpFolder,dbName,tb))

            sqlWriter(bkpFolder,dbName,sqlFileList)

        except OSError:
            print("Creation of the TABLE --{}-- failed".format(os.path.splitext(tb)[0]))

def sqlWriter(bkpFolder,dbName,sqList):
    sqlFileName = dbName + '_BKP.sql'
    pathFile = os.path.join(bkpFolder,dbName,sqlFileName)
    with open(pathFile, 'w') as f:
        for ln in sqList:
            f.write('{}\n\n'.format(ln))

def InsertIntoTable(file,db):
    f=open(file)
    dbName=os.path.basename(f.name)
    tblname=dbName.replace(".csv","")
    
    l=readSplitFile_1(file)
    
    insertData="INSERT INTO "+tblname+ "("
    for i in range (len(l[0])-1):
        insertData+=l[0][i+1]
        if i<len(l[0])-2:
            insertData+=","
        else:
            insertData+=") "
    insertData+="Values ("
    insertValues=""
    
    DataType=l[1]
    for row in range (len(l)-2):
        for col in range (len(l[row+2])-1):
            if typeValidation(DataType[col+1],l[row+2][col+1]):
                if DataType[col+1] in ['mac','ip','str']:
                    insertValues += "'"
                    insertValues += l[row+2][col+1].replace('\n','')
                    insertValues += "'"
                else:
                    insertValues+=(l[row+2][col+1]).replace('\n','')
                if col < (len(l[row+2])-2):
                    insertValues+=","
                else:
                    insertValues+=");"
        
        insertQuery=insertData+insertValues
        print(insertQuery)
        
        try:
            connection = mysql.connector.connect(host='localhost',
                                                database=db,
                                                user='root',
                                                password='oxmachines')

            cursor = connection.cursor()
            cursor.execute(insertQuery)
            connection.commit()
            print("Record inserted successfully into {} table".format(tblname))
            cursor.close()

        except mysql.connector.Error as error:
            print("Failed to insert record into {} table {}".format(tblname,error))

        finally:
            if (connection.is_connected()):
                connection.close()
                print("MySQL connection is closed\n")
        insertValues=""
    return 1    

def checkTable(conn,bkpFolder,dbName,csvName):

    useDatabase(conn,dbName)

    print('\t DATABASE: --{}--'.format(dbName))
  
    db_existence = 'SHOW TABLES;'
    cursor = queryExe(conn,db_existence)
    ex_result = cursor.fetchall()
    resultList = [i[0] for i in ex_result]

    tableName = os.path.splitext(csvName)[0]

    if tableName in resultList:
        print('Table --{}-- exists!\n'.format(tableName))
    else:
        print('Table --{}-- does not exists'.format(tableName))
        print('Creating it...\n')
        createTable(conn,bkpFolder,dbName,csvName)

#COMMAND LINE FUNCTIONS

def validDB():

    bkpFolder,dbNameList = dbargs()

    dbc = conn(host,user,password)

    for dbName in dbNameList:
        #dropDatabase(dbc,dbName)
        sqlCreateDB(dbc,bkpFolder,dbName)
        
        

def validTB():

    bkpFolder, dbNameList = dbargs()
    dbc = conn(host,user,password)

    for dbName in dbNameList:
        tabPath = os.path.join(bkpFolder,dbName)
        tabList = []

        for names in os.listdir(tabPath):
            if names.endswith('.csv'):
                tabList.append(names) 

        for csvName in tabList:
            checkTable(dbc,bkpFolder,dbName,csvName)


if __name__ == "__main__":
    if Arg[1] == 'DBValidate':
        validDB()
    elif Arg[1] == 'TBValidate':
        validTB()