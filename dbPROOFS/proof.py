import os
import re 


def ipValidation(ip_adress):
    ipRegExp = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(
                    25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$'''
    
    if re.search(ipRegExp,ip_adress):
        print("\nYou have entered a valid IP\n")
        return True
    else:
        print("\nInvalid IP, Please enter a valid one...\n")
        return False 

def macAdressValidation(mac_adress):
    macRegExp_1 = '''^([0-9A-F]{2}[-:]){5}[0-9A-F]{2}$'''
    macRegExp_2 = '''^([0-9a-f]{2}[-:]){5}[0-9a-f]{2}$'''

    if re.search(macRegExp_1,mac_adress) or re.search(macRegExp_2,mac_adress):
        print("\nYou have entered a valid MAC ADRESS\n")
        return True
    else:
        print("\nInvalid MAC ADRESS, Please enter a valid one...\n")
        return False

def boolValidation(val):
    if val == '1' or val == '0' or val == 'True' or val == 'False':
        return True
    else:
        print("\nInvalid BOOL value\n" )
        return False

def intValidation(val):
    try:
        int(val)
    except ValueError:
        return("Invalid int value")
    else:
        return("Data validated")

def stringValidation(val):
    if isinstance(val,str):
        return True
    else:
        return False

def dbValidation(FolderPath,db_name):
    dbPath = os.path.join(FolderPath,db_name)
    return os.path.isdir(dbPath)   

def tblValidation(file):
    try:
        with open(file,'r') as f:
            l = [[str(num) for num in line.split(',')] for line in f]
            fields=len(l[0])
            for i in range(len(l)):
                if (len(l[i])!=fields):
                    print("ERROR: Campos corruptos")
                    return 0
    except:
        print("ERROR: Table doesn't exist")   
        return 0
    else:
        f=open(file)
        fn=os.path.basename(f.name)
        print("Ready to load table: ",fn,"\n")    
        return 1

def tblDelete(file):
    try:
        os.remove(file)
    except OSError:
        print("ERROR: Couldn't delete file %s."%file)
    else:
        print("Succefully deleted the table %s."%file)

def SearchProject(pjname):

    start=os.getcwd()
    for roots,dirs,files in os.walk(start):
        if pjname in dirs:
            return 1,os.path.join(roots,pjname);
    return 0,("ERROR: Project directory doesn't exist",pjname);

def readFile(pathFile):
    f = open(pathFile,'r')
    l = f.readlines()
    return l

<<<<<<< HEAD
def dbCreator(FolderPath, db_name,TableRef):
    print(FolderPath,"\n", db_name,TableRef)
=======
def dbCreator(FolderPath, db_name):
>>>>>>> master
    if dbValidation(FolderPath,db_name):
        return print('\nThe DATABASE already exists\n')

    else:

        print('\nDATABASE does not exists\n\n')
        print('Creating new one from an older BACKUP...\n')

        if os.listdir(FolderPath) != []:

            try:
                path = os.path.join(FolderPath,db_name)
                os.mkdir(path)
                print('Folder database --%s-- created succesfully' %db_name)

            except OSError:
                print("Creation of the directory --%s-- failed" %path)

            else:
                print('Creating DATABASE TABLES...\n')
                try:
<<<<<<< HEAD
                    tablesPath = TablesRef
                    tableFilesName = os.listdir(TablesRef)
=======
                    tablesPath = os.path.join(FolderPath,'IO')
                    tableFilesName = os.listdir(tablesPath)
>>>>>>> master
                    for f in tableFilesName:

                        filePath =  os.path.join(tablesPath,f)
                        tableContent = readFile(filePath)
                        
                        pathNewTable = os.path.join(path,f)

                        newTable = open(pathNewTable,"w+")
                        newTable.writelines(tableContent)
                        newTable.close()
                        
                        print('Table --%s-- created succesfully \n' %f)

                except OSError:
                    print("Creation of the database table --%s-- failed" %newTable)

        else:
            print('Error: There are no database backup files...')

def tblCreator(bkpRoot,bdRoot,tblname):
    try:
<<<<<<< HEAD
        tablesPath=bkpRoot
=======
        tablesPath = os.path.join(bkpRoot,'IO')
>>>>>>> master
        tableFilesName = os.listdir(tablesPath)

        if tblname in tableFilesName:
            filePath =  os.path.join(tablesPath,tblname)
            tableContent = readFile(filePath)

            pathNewTable = os.path.join(bdRoot,tblname)

            newTable = open(pathNewTable,"w+")
            newTable.writelines(tableContent)
            newTable.close()
            
            print('Table --%s-- created succesfully\n' %tblname)

    except OSError:
        print("Creation of the database table --%s-- failed\n" %newTable)

def typeValidation(type,val):
    if type == 'ip':
        return ipValidation(val)
    elif type == 'mac':
        return macAdressValidation(val)
    elif type == 'bool':
        return boolValidation(val)
    elif type == 'int':
        return intValidation(val)
    elif type == 'str':
        return stringValidation(val)
    else:
        print('No type variable found...')

def readSplitFile(pathFile):
    f = open(pathFile,'r') 
    l = [[str(num) for num in line.split(',')] for line in f]
    return l

def dataValidation(dbPath,tableNameFile,RefdbPath):
    tablePath = os.path.join(dbPath,tableNameFile)

    valFilePath = os.path.join(RefdbPath,'valType'+tableNameFile)

    tableContent = readSplitFile(tablePath)
    valTableContent = readSplitFile(valFilePath)

    validationTable = [[]]

    for row in range(len(tableContent)):

        if row == 0:
            continue

        else:
            for col in range(len(tableContent[0])):
                
                type = valTableContent[0][col]
                result = typeValidation(type,val)

                if result:
                    continue
                else:
                    print("There is wrong type data indexed in this row...")
                return 0

    return validationTable


#Nombre de la carpeta del proyecto
pjname="17066.SIGMA"
#Nombre de la base de datos a buscar, si no se encuentra crea una nueva en base a la
#carpeta de referencia IO y sus archivos.
<<<<<<< HEAD
print("\n***********************************************************************************************************")
print("Nota: La carpeta IO es en donde se encuetran los archivos de referencia para crear nuevas bases de datos y tablas")
print("************************************************************************************************************\n\n")
dbname=input("Enter Data Base Name: ")
=======
dbname="IOc"
>>>>>>> master

#Archivos de referencia, simulan que son los de la base de datos
filesnames=['Control.txt','Status.txt','Inputs.txt','InputsMapping.txt','Outputs.txt','OutputsMapping.txt']

<<<<<<< HEAD

pjExist,pjRoot=SearchProject(pjname)
TablesRef=os.path.join(pjRoot,"DB.BKP/IO/TablesStructure")
DataRef=os.path.join(pjRoot,"DB.BKP/IO/DataStructure")
=======
pjExist,pjRoot=SearchProject(pjname)
>>>>>>> master
if pjExist:
    if dbValidation(pjRoot,"DB.BKP"):
        bkpRoot=os.path.join(pjRoot,"DB.BKP")
        if dbValidation(bkpRoot,dbname):
            dbRoot=os.path.join(bkpRoot,dbname)
            files=os.listdir(dbRoot)
            for i in range(len(filesnames)):
                if filesnames[i] in files:
                    print("\nVerifying table... ")
                    if tblValidation(os.path.join(dbRoot,filesnames[i])) == 0:
                        print("ERROR:Found an inconcistence in the table structure")
                        print("Table %s wil be deleted" %files[i])
                        tblDelete(os.path.join(dbRoot,filesnames[i]))
                        print("Making new %s table..."%filesnames[i])
<<<<<<< HEAD
                        tblCreator(TablesRef,dbRoot,filesnames[i])

                    if dataValidation(dbRoot,filesnames[i],DataRef) == 0:
                        print("Table %s wil be deleted" %files[i])
                        tblDelete(os.path.join(dbRoot,filesnames[i]))
                        print("Making new %s table..."%filesnames[i])
                        tblCreator(TablesRef,dbRoot,filesnames[i])
                else:
                    print("Table does not exist %s"%filesnames[i])
                    print("Creating new one from an older BACKUP...")
                    tblCreator(TablesRef,dbRoot,filesnames[i])
                

        else:
            dbCreator(bkpRoot,dbname,TablesRef)        
=======
                        tblCreator(bkpRoot,dbRoot,filesnames[i])

                    if dataValidation(dbRoot,filesnames[i],os.path.join(bkpRoot,"IO")) == 0:
                        print("Table %s wil be deleted" %files[i])
                        tblDelete(os.path.join(dbRoot,filesnames[i]))
                        print("Making new %s table..."%filesnames[i])
                        tblCreator(bkpRoot,dbRoot,filesnames[i])
                else:
                    print("Table does not exist %s"%filesnames[i])
                    print("Creating new one from an older BACKUP...")
                    tblCreator(bkpRoot,dbRoot,filesnames[i])
                

        else:
            dbCreator(bkpRoot,dbname)        
>>>>>>> master
    else:
        print("ERROR: BACKUP doesn't exist")
else:
    print(pjRoot)                   
